package com.darioxlz.bnamarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BnaMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(BnaMarketApplication.class, args);
	}

}
